package com.mapswithme.maps.location;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.RemoteNotificationCompat;
import com.mapswithme.maps.MwmActivity;
import com.mapswithme.maps.MwmApplication;
import com.mapswithme.maps.R;
import com.mapswithme.util.ThemeUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RecordingService extends PersistentService {
    public static final String TAG = RecordingService.class.getSimpleName();

    public static final int NOTIFICATION_RECORDING_ICON = 1;

    public static final String RECORDING = "recording";

    public static String SHOW_ACTIVITY = RecordingService.class.getCanonicalName() + ".SHOW_ACTIVITY";
    public static String PAUSE_BUTTON = RecordingService.class.getCanonicalName() + ".PAUSE_BUTTON";
    public static String STOP_BUTTON = RecordingService.class.getCanonicalName() + ".STOP_BUTTON";

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");

    static {
        NOTIFICATION_PERSISTENT_ICON = NOTIFICATION_RECORDING_ICON;
    }

    RecordingReceiver receiver;
    Handler handler;
    Runnable refresh = new Runnable() {
        @Override
        public void run() {
            refresh();
        }
    };

    public class RecordingReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                // showRecordingActivity();
            }
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                // do nothing. do not annoy user. he will see alarm screen on next screen on event.
            }
        }
    }

    public static boolean isRecording(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        long time = shared.getLong(RECORDING, 0);
        return time != 0;
    }

    public static void startIfEnabled(Context context) {
        if (!isRecording(context))
            return;
        startService(context);
        MwmActivity.startHelper(context);
    }

    public static void startService(Context context) {
        if (!isRecording(context)) {
            SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor edit = shared.edit();
            edit.putLong(RECORDING, System.currentTimeMillis());
            edit.commit();
            TrackRecorder.nativeClearTrack();
        }
        start(context, new Intent(context, RecordingService.class));
    }

    public static void updateService(Context context) {
        start(context, new Intent(context, RecordingService.class));
    }

    public static void stopService(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        long time = shared.getLong(RECORDING, 0);
        SharedPreferences.Editor edit = shared.edit();
        edit.putLong(RECORDING, 0);
        edit.commit();
        stop(context, new Intent(context, RecordingService.class));
        MwmActivity.stopRecording(context, time);
    }

    public RecordingService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onCreateOptimization() {
        optimization = new PersistentService.ServiceReceiver(this, getClass(), PREFERENCE_OPTIMIZATION) {
            @Override
            public void register() { // do not call super (we not using optimization option)
                next();
                am.set(next, OptimizationPreferenceCompat.serviceCheck(context, service));
                OptimizationPreferenceCompat.setKillCheck(RecordingService.this, next, PREFERENCE_NEXT);
            }

            @Override
            public void unregister() {
                super.unregister();
                OptimizationPreferenceCompat.setKillCheck(RecordingService.this, 0, PREFERENCE_NEXT);
            }
        };
        optimization.create();
        MwmApplication app = (MwmApplication) getApplication();
        if (!app.arePlatformAndCoreInitialized())
            app.initCore();
        handler = new Handler();
        receiver = new RecordingReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(receiver, filter);
        refresh();
    }

    @Override
    public void onStartCommand(Intent intent) {
        String a = intent.getAction();
        if (a == null) {
            updateIcon();
        } else if (a.equals(STOP_BUTTON)) {
            stopService(this);
        } else if (a.equals(PAUSE_BUTTON)) {
            MwmActivity.pauseRecording(this);
        } else if (a.equals(SHOW_ACTIVITY)) {
            MwmActivity.startActivity(this);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");
        unregisterReceiver(receiver);
        handler.removeCallbacks(refresh);
    }

    @Override
    public Notification build(Intent intent) {
        PendingIntent main = PendingIntent.getService(this, 0,
                new Intent(this, RecordingService.class).setAction(SHOW_ACTIVITY),
                PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pe1 = PendingIntent.getService(this, 0,
                new Intent(this, RecordingService.class).setAction(PAUSE_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pe2 = PendingIntent.getService(this, 0,
                new Intent(this, RecordingService.class).setAction(STOP_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        long time = shared.getLong(RECORDING, 0);

        NotificationChannelCompat channel = new NotificationChannelCompat(this, "recording", "Recording", NotificationManagerCompat.IMPORTANCE_LOW);

        RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(this, R.layout.notification);
        String title = getString(R.string.app_name);
        String text = SIMPLE_DATE_FORMAT.format(new Date(time)) + " - " + (TrackRecorder.isEnabled() ? "recording" : "paused");
        builder.setImageViewResource(R.id.notification_pause, TrackRecorder.isEnabled() ? R.drawable.ic_pause_black_48dp : R.drawable.ic_fiber_manual_record_red_48dp);
        builder.setOnClickPendingIntent(R.id.notification_pause, pe1);
        builder.setOnClickPendingIntent(R.id.notification_stop, pe2);

        builder.setTitle(title)
                .setText(text)
                .setMainIntent(main)
                .setTheme(ThemeUtils.isNightTheme() ? R.style.MwmTheme_Night : R.style.MwmTheme)
                .setChannel(channel)
                .setWhen(notification)
                .setSmallIcon(R.drawable.ic_recording)
                .setOngoing(true);

        if (!TrackRecorder.isEnabled())
            builder.setImageViewTint(R.id.notification_pause, Color.RED);

        return builder.build();
    }

    @Override
    public void updateIcon() {
        super.updateIcon(new Intent());
    }

    void refresh() {
        handler.removeCallbacks(refresh);
        updateIcon();
        handler.postDelayed(refresh, 10 * 1000);
    }
}
