package com.mapswithme.maps.analytics;

import android.app.Application;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mapswithme.maps.BuildConfig;
import com.mapswithme.util.log.Logger;
import com.mapswithme.util.log.LoggerFactory;

import java.util.Collections;
import java.util.Map;

class LibnotifyEventLogger extends DefaultEventLogger
{
  @SuppressWarnings("NullableProblems")
  LibnotifyEventLogger(@NonNull Application application)
  {
    super(application);
  }

  @Override
  public void initialize()
  {
  }

  @Override
  public void sendTags(@NonNull String tag, @Nullable String[] params)
  {
  }
}
