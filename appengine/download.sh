#!/bin/bash
#

set -e

# define those vaiables in bash before startin:
#
# export URL=http://opensource-data.mapswithme.com/regular/weekly/
# export DROPBOX=dropboxtoken
# export MAP=190113

IFS_old=$IFS
IFS=$'\n'
WORLD=( $(cat ../data/external_resources.txt | awk '{print $1}') "external_resources.txt" "skipped_elements.lst" )
IFS=$IFS_old

# data/borders/*.poly -> *.mwm + countires.txt
#
IFS_old=$IFS
IFS=$'\n'
MAPS=( $(cd ../data/borders && for i in *.poly; do echo ${i%.*}.mwm; done) "countries.txt" )
IFS=$IFS_old

LIST=( "${WORLD[@]}" "${MAPS[@]}" )

get_temporary_upload_link() {
	local A="$1"
	curl --silent -X POST "https://api.dropboxapi.com/2/files/get_temporary_upload_link" \
	    --header "Authorization: Bearer $DROPBOX" \
	    --header "Content-Type: application/json" \
	    --data "{\"commit_info\": {\"path\": \"$A\",\"mode\": \"overwrite\",\"autorename\": false,\"mute\": false,\"strict_conflict\": false},\"duration\": 3600}" | jq -r '.link'
}

dropbox_upload() {
	local URL="$1"
	curl --silent -X POST "$URL" \
		--header "Content-Type: application/octet-stream" \
		--data-binary @- > /dev/null
}

googledrive_upload_link() {
	local A="$1"
	local ID="folderid"
	curl --silent -X POST "https://www.googleapis.com/drive/v3/files?key=$GOOGLE" \
	    --header 'Accept: application/json' \
	    --header 'Content-Type: application/json' \
	    --data "{\"name\":\"$A\",\"parents\":[\"$ID\"]}"
}

googledrive_upload() {
	local ID="$1"
	curl --silent -X PATCH "https://www.googleapis.com/upload/drive/v3/files/$ID?key=$GOOGLE" \
		--header "Content-Type: application/octet-stream" \
		--data-binary @- > /dev/null
}

for (( i=0; i<${#LIST[@]}; i++ )); do
	M="${LIST[$i]}"
	if [ -n "$DROPBOX" ]; then
		echo "$M"
		U=$(get_temporary_upload_link "/$MAP/$M")
		wget -qO - "$URL/$M" | dropbox_upload "$U"
	elif [ -n "$GOOGLE" ]; then
		echo "$M"
		U=$(googledrive_upload_link "/$MAP/$M")
		wget -qO - "$URL/$M" | googledrive_upload "$U"
	else
		wget "$URL/$M"
	fi
done
